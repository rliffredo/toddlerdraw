﻿using System;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Globalization;

namespace Painters
{
    /// <summary>
    /// Base class for all drawing logic
    /// </summary>
    class Base
    {
        protected Path _pathPainter;
        protected Canvas _container;

        public Base(Canvas cv, BrushInfo binfo) {
            _container = cv;
            InitializePath(binfo);
        }

        protected virtual void InitializePath(BrushInfo binfo) {
            _pathPainter = new Path();
            _pathPainter.StrokeThickness = binfo.StrokeThickness;
            _container.Children.Add(_pathPainter);
            UpdateColor(binfo);
        }

        public virtual void UpdateColor(BrushInfo binfo) {
            _pathPainter.Stroke = binfo.CurrentColor();
            _pathPainter.Fill = binfo.CurrentColor();
        }

    }

    /// <summary>
    /// Manages drawing a circle under the cursor
    /// </summary>
    class Cursor : Base
    {
        public Cursor(Canvas cv, BrushInfo binfo)
            : base(cv, binfo) {
            EllipseGeometry geoCursor = new EllipseGeometry();
            geoCursor.RadiusX = 10;
            geoCursor.RadiusY = 10;
            _pathPainter.Data = geoCursor;
            UpdatePosition(new Point(-50, -50));
        }

        public void UpdatePosition(Point position) {
            EllipseGeometry geoCursor = (EllipseGeometry)_pathPainter.Data;
            geoCursor.Center = position;
        }

    }

    /// <summary>
    /// Manages text drawing
    /// </summary>
    class Text : Base
    {
        String _curMsg = "";

        public Text(Canvas cv, BrushInfo binfo)
            : base(cv, binfo) {
        }

        protected override void InitializePath(BrushInfo binfo) {
            base.InitializePath(binfo);
            _pathPainter.StrokeThickness = 1;
        }

        private bool UpdateText(String newMsg) {
            FormattedText text = new FormattedText(newMsg,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Tahoma"),
                72,
                Brushes.Black);
            text.MaxTextWidth = _container.ActualWidth;
            Geometry geometry = text.BuildGeometry(new Point(5, 5));
            PathGeometry pathGeometry = geometry.GetFlattenedPathGeometry();
            if (pathGeometry.Bounds.Height > _container.ActualHeight) {
                System.Media.SystemSounds.Asterisk.Play();
                return false;
            } else {
                _pathPainter.Data = pathGeometry;
                return true;
            }
        }

        public void removeLastChar() {
            if (_curMsg.Length > 0) {
                _curMsg = _curMsg.Remove(_curMsg.Length - 2); // Remove ZWSP as well
                UpdateText(_curMsg);
            }
        }

        public void appendNewChar(String newChar) {
            // Note: always append a ZWSP to allow breaking in any place
            String newMsg  = _curMsg + newChar.ToUpper() + '\u200B';
            if (UpdateText(newMsg)) {
                _curMsg = newMsg;
            }
        }

        static private String appendNewChar(String newChar, String curMsg) {
            String newMsg;
            if (newChar == "\b") {
                if (curMsg.Length > 0) {
                    newMsg = curMsg.Remove(curMsg.Length - 1);
                } else {
                    newMsg = curMsg;
                }
            } else {
                newMsg = curMsg + newChar.ToUpper();
            }
            return newMsg;
        }

    }

    /// <summary>
    /// Manages sketch drawing
    /// </summary>
    class Sketch : Base
    {
        PolyLineSegment _polyLine;

        public Sketch(Canvas cv, BrushInfo binfo)
            : base(cv, binfo) {
        }

        protected override void InitializePath(BrushInfo binfo) {
            base.InitializePath(binfo);
            _pathPainter.Data = new PathGeometry();

            PathFigure drawing = new PathFigure();

            _polyLine = new PolyLineSegment();
            _polyLine.IsSmoothJoin = true;
            drawing.Segments.Add(_polyLine);

            PathGeometry pathGeometry = (PathGeometry)_pathPainter.Data;
            pathGeometry.Figures.Add(drawing);
        }

        public override void UpdateColor(BrushInfo binfo) {
            _pathPainter.Stroke = binfo.CurrentColor();
        }

        public void AddNewSegment(Point position) {
            if (_polyLine.Points.Count == 0) {
                PathGeometry pathGeometry = (PathGeometry)_pathPainter.Data;
                pathGeometry.Figures[0].StartPoint = position;
            }
            _polyLine.Points.Add(position);
        }

        public void AddNewSection(BrushInfo binfo) {
            InitializePath(binfo);
        }

    }

    /// <summary>
    /// Maintains information about brush used, along with ways to update it
    /// </summary>
    abstract class BrushInfo
    {
        public int StrokeThickness = 10;
        public abstract Brush CurrentColor();
        public abstract Brush NextColor();
    }

    class ColorBrushInfo: BrushInfo {
        private int _curStroke;
        private Brush[] _strokes = {Brushes.Black, Brushes.Red, Brushes.Orange,
            Brushes.Yellow, Brushes.Green, Brushes.Blue};

        public ColorBrushInfo() {
            _curStroke = 0;
        }

        public override Brush CurrentColor() {
            return _strokes[_curStroke];
        }

        public Brush SetColor(int nColorPos) {
            if (nColorPos < 0) {
                nColorPos = 0;
            } else if (nColorPos > _strokes.Length) {
                nColorPos = _strokes.Length;
            }
            _curStroke = nColorPos;
            return CurrentColor();
        }

        public override Brush NextColor() {
            _curStroke = (_curStroke + 1) % _strokes.Length;
            return CurrentColor();
        }

    }

    class TransparentBrushInfo : BrushInfo
    {
        public override Brush CurrentColor() {
            return Brushes.Transparent;
        }
        public override Brush NextColor() {
            return Brushes.Transparent;
        }
    }
}

namespace ToddlerDraw
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        bool _isPainting = true;
        Painters.ColorBrushInfo _currentBrush;
        Painters.Cursor _cursor;
        Painters.Text _text;
        Painters.Sketch _drawing;

        private void InitializePainters() {
            _text = new Painters.Text(canvas, new Painters.ColorBrushInfo());
            _cursor = new Painters.Cursor(canvas, _currentBrush);
            _drawing = new Painters.Sketch(canvas, _currentBrush);
        }

        public MainWindow() {
            InitializeComponent();
            _currentBrush = new Painters.ColorBrushInfo();
            InitializePainters();
        }

        private void window_MouseMove(object sender, MouseEventArgs e) {
            // Paint under the mouse
            Point curpos = e.GetPosition(this);
            this.Title = curpos.ToString();
            if (!_isPainting) {
                return;
            }
            _cursor.UpdatePosition(curpos);
            _drawing.AddNewSegment(curpos);
        }

        private void window_MouseRightButtonUp(object sender, MouseButtonEventArgs e) {
            // Erase everything
            canvas.Children.Clear();
            InitializePainters();
        }

        private void window_TextInput(object sender, TextCompositionEventArgs e) {
            // Write text
            if (e.ControlText.Length > 0) {
                return;
            }
            _text.appendNewChar(e.Text);
            e.Handled = true;
        }

        private void window_KeyDown(object sender, KeyEventArgs e) {
            // Tab - pause drawing
            // Backspace - delete last character
            // F1-F6 - set color
            // Esc - Switch color
            if (e.Key == Key.Tab) {
                _isPainting = !_isPainting;
                if (_isPainting) {
                    _cursor.UpdatePosition(Mouse.GetPosition(this));
                    _cursor.UpdateColor(_currentBrush);
                    _drawing.AddNewSection(_currentBrush);
                } else {
                    _cursor.UpdateColor(new Painters.TransparentBrushInfo());
                }
                e.Handled = true;
            } else if (e.Key == Key.Back) {
                _text.removeLastChar();
                e.Handled = true;
            } else if (e.Key >= Key.F1 && e.Key <= Key.F6) {
                int nColorId = e.Key - Key.F1;
                Brush oldColor = _currentBrush.CurrentColor();
                Brush newColor = _currentBrush.SetColor(nColorId);
                if (_isPainting &&  newColor != oldColor) {
                    _cursor.UpdateColor(_currentBrush);
                    _drawing.AddNewSection(_currentBrush);
                }
                e.Handled = true;
            } else if (e.Key == Key.Escape) {
                _currentBrush.NextColor();
                if (_isPainting) {
                    _cursor.UpdateColor(_currentBrush);
                    _drawing.AddNewSection(_currentBrush);
                }
                e.Handled = true;
            }
        }

    }
}
