
# ToddlerDraw

ToddlerackDraw is an extremely simplified drawing software, that helps developing drawing and reading/writing skills in small toddlers.

It was born out of two needs I had: quickly securing the PC when my daughter wanted to "work on it" sitting on my knees, and helping her developing her skills.

It was thought to be a tool: it is not something to replace an adult teaching the kid, but more to assist in the job. For instance, there is no automatic reading of the letters, as they are being typed, because I think it is parent's job to actually sit with the kid and teach them new things.

Go to usage for more details about the features and the user interface.


## Usage

Usage is (should be) very simple, with a very limited feature set:

- To draw, just move the mouse (or swipe the touchpad)
- ESC key cycle between drawing colors; F1-F6 to jump to one, directly
  - Available colors: Black, Red, Orange, Yellow, Green, Blue 
- Right click to erase the entire image
- Press any key to draw on the screen
  - No automatic wrapping (must press enter)
  - Cannot write outside of the screen boundary
  - Always write in black
  - No editing, only backspace may be used to delete characters back
  - Text is erased together with graphic 
- Press TAB key to switch off drawing - useful to draw more complex figures
- Press Alt-F4 to exit the application 

## Installation

ToddlerDraw requires .NET Framework 3.5 - I think this should be quite common, nowadays.  
No installation is required; just download it from the Downloads section, and run it.

